% generates photon time and energy list
% energy in KeV, time in microsecond
% inputs:
%  rad_dist_in = radial distance in km between detector and TGF source.
%  eff_area = effective area of the detector in cm2, default to 100
%  source_brightness_scale = scale of the TGF source relative to 10^17. Default value is 1. To be set to 0.1 for 10^16, 10 for 10^18, ...
% outputs:
%   time_list : photon time list in microsecond
%   energy_list : energy list in keV, minimum is set to 50 keV
%   nb_to_generate : number that was generated (length of time_list and energy_list)

% It also produces a .txt file is located insde folder ./output/ with the time and energy lists

function [time_list,energy_list,nb_to_generate]=generate_TGF_photons(rad_dist_in,source_brightness_scale,eff_area,source_t_sigma)

if nargin==1
    source_brightness_scale = 1; % 1 for 10^17, 0.1 for 10^16, 10 for 10^18, ...
    eff_area=100; % cm2
    source_t_sigma = 1; % us
elseif nargin==2
    eff_area = 100; % 1 for 10^17, 0.1 for 10^16, 10 for 10^18, ...
    source_t_sigma = 1; % us
elseif nargin==3
    source_t_sigma = 1; % us
end

if source_t_sigma<0
   error('source_t_sigma should be >= 0 us') 
end
if eff_area<0
   error('eff_area should be >= 0 cm2') 
end
if source_brightness_scale<0
   error('source_brightness_scale should be >= 0') 
end
if rad_dist_in<0
   error('rad_dist_in should be >= 0 km') 
end

! mkdir -p output

load('database.mat','data');

if rad_dist_in>max(data.RD_profile.rad_dist)
    error(['Radial distance of ' num2str(rad_dist_in) ' km is too high. Please reduce.'])
end

nb_to_generate = interp1(data.RD_profile.rad_dist,data.RD_profile.fluence,rad_dist_in)*eff_area;

nb_to_generate = round(nb_to_generate);

disp(['To generate: ' num2str(nb_to_generate) ' photons.'])

if nb_to_generate==0
   error('No photon generated (try reducing radial distance, or increasing source_brightness_scale)') 
end

rd_bins = logspace(log10(min(data.rad_dist_list)),log10(max(data.rad_dist_list)),16);

[min_edge,max_edge] = find_edges(rad_dist_in,rd_bins);

to_keep = data.rad_dist_list>=min_edge & data.rad_dist_list<max_edge;

data.time = data.time(to_keep);
data.energy = data.energy(to_keep);

if nb_to_generate>500000
    error(['Too much photon to generate (' num2str(nb_to_generate) '). Try reducing source_brightness_scale or eff_area; or reduce Rad_dist']);
end

if nb_to_generate>length(data.energy)
    data = demultiply_data(data,nb_to_generate);
end

% get the randomly-selected indices
indices = randperm(length(data.energy));
indices = indices(1:nb_to_generate);
% choose the subset of a you want
time_list = data.time(indices);
time_list = time_list(:)';
energy_list = data.energy(indices);
energy_list = energy_list(:)';

if source_t_sigma>0
    time_list = time_list + normrnd(0,source_t_sigma,size(time_list));
    time_list = time_list-min(time_list);
end

% sort by increasing time and set first time to 0

[~,is] = sort(time_list);

time_list = time_list(is) - min(time_list);
energy_list = energy_list(is);

%% write to txt file

filename = ['./output/' 'generated_photons_' num2str(rad_dist_in) '_' num2str(source_brightness_scale) '_' num2str(eff_area) '.txt'];

% writematrix([time_list(:) energy_list(:)],filename,'Delimiter',' ');

str1=[num2str([time_list(:)], '%6.3e') repmat('  ',[length(energy_list) 1]) num2str([energy_list(:)],'     %6.3e')];

dlmwrite(filename,str1, 'delimiter', '');

add_header(filename,'Time (microsecond)  Energy (keV)');

disp(['Generated ' num2str(nb_to_generate) ' photons inside file: ' filename])


end

%%

function [min_edge,max_edge] = find_edges(rad_dist_in,rd_bins)

for ii=1:length(rd_bins)-1
    
    min_edge = rd_bins(ii);
    max_edge = rd_bins(ii+1);
    
    if rad_dist_in>=min_edge && rad_dist_in<max_edge
        return;
    end
    
end

end

%%
function data = demultiply_data(data,nb_to_generate)

to_repeat = ceil(nb_to_generate/length(data.energy));

data.energy=data.energy(:);
data.time=data.time(:);
data.rad_dist_list=data.rad_dist_list(:);

data.energy = repmat(data.energy,[to_repeat 1]);
data.time = repmat(data.time,[to_repeat 1]);
data.rad_dist_list = repmat(data.rad_dist_list,[to_repeat 1]);

data.energy = normrnd(data.energy,(data.energy)/40);
data.time = normrnd(data.time,(data.time)/40);

end

%%
function add_header(filename,header_txt)

% Read the file
fid = fopen(filename,'r');
str = textscan(fid,'%s','Delimiter','\n');
fclose(fid);
% Add your header
str2 = [{header_txt}; str{1}];
% Save as a text file
fid2 = fopen(filename,'w');
fprintf(fid2,'%s\n', str2{:});
fclose(fid2);

end
