*Program/function to generate list of photons detected at 20 km altitude. The number of photons generated is relative to the fluence at the given radial distance (assuming initial TGF is made of 10^17 photons, there is a scale factor as an optional parameter of the function.)*


****Description of the MATLAB function :****
`[time_list,energy_list,nb_to_generate]=generate_TGF_photons(rad_dist_in,source_brightness_scale,eff_area,source_t_sigma)`
 * generates photon time and energy list. energy in keV, time in microsecond. It also produce a `.txt` file is located insde folder `./output/`
 * inputs:
     * `rad_dist_in` = **Must be provided.** radial distance in km between detector and TGF source.
     * `source_brightness_scale` = **Optional**. scale of the TGF source relative to 10^17. *Default value is 1.* To be set to 0.1 for 10^16, 10 for 10^18, ...
     * `eff_area` = **Optional**. effective area of the detector in cm2. *Default value is 100.*
     * `source_t_sigma` = **Optional**. time (micro second), sigma of a Gaussian (normal) distribution of the TGF source duration. *Default value is 1 us*.
 * outputs:
     * `time_list` : TGF photon time list in microsecond
     * `energy_list` : TGF photon energy list in keV, minimum is set to 50 keV
     * `nb_to_generate` : number that was generated (length/size of `time_list` and `energy_list`)
 * See `example.m` that show it run and plot the time-energy values of the TGF photons as form of a scatter plot.


****Simulation details:****
* TGF simulation properties:
    * Production altitude of 12 km, latitude = 13 deg, and longitude = -79 deg
    * minimum generated energy of 500 keV (but records can have energy down to 50 keV) because the probability of reaching 20 km for lower energies (starting at 12 km) is close to 0.
    * Gaussian opening angle distribution centered around nadir, with sigma = 15 deg
    * record energy >= 50 keV
    * instantaneous time distribution at source (i.e. all photons start with t=0 at 12 km), Gaussian smearing is added later during the MATLAB processing.
* The GEANT4-based code is available here: https://github.com/DavidSarria89/TGF-Propagation-Geant4 and was slightly modified for the occasion.