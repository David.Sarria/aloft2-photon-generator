clear all
close all
clc

figure(1)

% generate photons for radial distance of 10 km between TGF source and detector
% default parameters for scaling (1) and effective area (100)
subplot(2,2,1)
RD = 10;
[time_list,energy_list,nb_to_generate]=generate_TGF_photons(RD);
% plot result
figure(1)
scatter(time_list,energy_list)
set(gca,'yscale','log')
xlabel('Time (us)')
ylabel('Energy (keV)')
title(['Radial distance of ' num2str(RD) ' km. Default.'])
grid on

% generate photons for radial distance of 11 km between TGF source and detector
% Scaling parameter set to 100 (i.e. there are 10^19 photons at source) and default effective area
subplot(2,2,2)
RD = 11;
[time_list,energy_list,nb_to_generate]=generate_TGF_photons(RD,100);
% plot result
scatter(time_list,energy_list)
set(gca,'yscale','log')
xlabel('Time (us)')
ylabel('Energy (keV)')
title([{['Radial distance of ' num2str(RD) ' km.'], ['source scaling of 100']}])
grid on

% generate photons for radial distance of 12 km between TGF source and detector
% Scaling parameter set to 100 (i.e. there are 10^19 photons at source) and effective area of 1000 cm2
subplot(2,2,3)
RD = 12;
[time_list,energy_list,nb_to_generate]=generate_TGF_photons(RD,100,1000);
% plot result
scatter(time_list,energy_list)
set(gca,'yscale','log')
xlabel('Time (us)')
ylabel('Energy (keV)')
title([{['Radial distance of ' num2str(RD) ' km.'], ['source scaling of 100'],['eff. area of 1000 cm2']}])
grid on

% generate photons for radial distance of 12 km between TGF source and detector
% Scaling parameter set to 100 (i.e. there are 10^19 photons at source) and effective area of 1000 cm2
subplot(2,2,4)
% TGF source duration gaussian with sigma of 10 us
RD = 12;
[time_list,energy_list,nb_to_generate]=generate_TGF_photons(RD,100,1000,10);
% plot result
scatter(time_list,energy_list)
set(gca,'yscale','log')
xlabel('Time (us)')
ylabel('Energy (keV)')
title([{['Radial distance of ' num2str(RD) ' km'], ['TGF source duration gaussian with sigma of 10 us']}])
grid on


saveas(gcf,'example.png')
